import React, {Fragment, useEffect} from 'react';
import {HashRouter as Router, Route, Switch} from "react-router-dom";
import {Container} from 'react-bootstrap';
import HeaderNavbar from './components/layout/HeaderNavbar';
import Landing from './components/layout/Landing';
import Alert from './components/layout/Alert';
import Login from './components/auth/Login';
import Register from './components/auth/Register';
import Dashboard from './components/dashboard/Dashboard';
import CreateProfile from './components/profile-forms/CreateProfile';
import PrivateRouting from './components/routing/PrivateRouting';
import {setAuthToken} from "./utils/setAuthToken";

//REDUX
import {Provider} from 'react-redux';
import store from './store';
import {loadUser} from "./actions/auth";

import './styling/_main.scss';

if(localStorage.token) {
    setAuthToken(localStorage.token);
}

const App = () => {
    useEffect(() => {
       store.dispatch(loadUser());
    }, []);

    return (
        <Provider store={store}>
            <Router>
                <Fragment>
                    <HeaderNavbar/>
                    <Route exact path='/' component={Landing}/>
                    <Container className='mt-5'>
                        <Alert/>
                        <Switch>
                            <Route exact path='/register' component={Register}/>
                            <Route exact path='/login' component={Login}/>
                            <PrivateRouting exact path='/create-profile' component={CreateProfile}/>
                            <PrivateRouting exact path='/dashboard' component={Dashboard}/>
                        </Switch>
                    </Container>
                </Fragment>
            </Router>
        </Provider>
    );
};

export default App;
