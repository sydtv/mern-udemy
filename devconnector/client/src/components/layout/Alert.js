import React from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {Alert as BootstrapAlert} from 'react-bootstrap';

const Alert = ({alerts}) =>
    alerts !== null &&
    alerts.length > 0 &&
    alerts.map((alert, id) => (
        <BootstrapAlert variant={alert.alertType} key={id}>
            {alert.msg}
        </BootstrapAlert>
    ));


Alert.propTypes = {
    alerts: PropTypes.array.isRequired
};

const mapStateToProps = state => ({
    alerts: state.alert
});

export default connect(mapStateToProps)(Alert);