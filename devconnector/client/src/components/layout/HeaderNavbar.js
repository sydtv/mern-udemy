import React, {Fragment} from 'react';
import {Link} from 'react-router-dom';
import {Container, Navbar as NavB, Nav} from 'react-bootstrap';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faSignOutAlt, faUser} from '@fortawesome/free-solid-svg-icons';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {logout} from "../../actions/auth";

const Navbar = ({auth: {isAuthenticated, loading}, logout}) => {

    const authLinks = (
        <Fragment>
            <Link className="nav-link" to='/dashboard'><FontAwesomeIcon icon={faUser} />{' '}Dashboard</Link>
            <Link className="nav-link" onClick={logout} to='/'><FontAwesomeIcon icon={faSignOutAlt} />{' '}Log Out</Link>
        </Fragment>
    );

    const guestLinks = (
        <Fragment>
            <Link className="nav-link" to='/login'>Developers</Link>
            <Link className="nav-link" to='/register'>Register</Link>
            <Link className="nav-link" to='/login'>Login</Link>
        </Fragment>
    );

    return (
        <NavB bg="light" expand="lg">
            <Container>
                <Link className="navbar-brand" to={isAuthenticated ? '/dashboard' : '/'}>DevConnector</Link>
                <NavB.Toggle aria-controls="headerNavBar"/>
                <NavB.Collapse id="headerNavBar">
                    <Nav className="ml-auto">
                        {loading === false && (<Fragment>{ isAuthenticated ? authLinks : guestLinks}</Fragment>)}
                    </Nav>
                </NavB.Collapse>
            </Container>
        </NavB>
    )
};

Navbar.propTypes = {
  logout: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth
});

export default connect(mapStateToProps, {logout})(Navbar);