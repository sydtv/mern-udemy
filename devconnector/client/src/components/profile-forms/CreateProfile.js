import React, {Fragment} from 'react';
import {Link, withRouter} from "react-router-dom";
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {createProfile} from "../../actions/profile";
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faYoutube, faInstagram, faTwitter, faFacebook, faLinkedin, faGithub} from '@fortawesome/free-brands-svg-icons';
import {Form as BootstrapForm, Button, InputGroup} from 'react-bootstrap';
import {Select} from "antd";
import {withFormik, Form, Field} from 'formik';
import * as Yup from 'yup';

const Option = Select.Option;

const statusOptions = [
    {value: 'developer', label: 'Developer'},
    {value: 'junior', label: 'Junior Developer'},
    {value: 'senior', label: 'Senior Developer'},
    {value: 'manager', label: 'Manager'},
    {value: 'student', label: 'Student or Learning'},
    {value: 'instructor', label: 'Instructor or Teacher'},
    {value: 'intern', label: 'Intern'},
    {value: 'other', label: 'Other'}
];

const CreateProfile = (props) => {
    return (
        <Fragment>
            <h1 className="mb-4">Create Profile</h1>
            <FormikCreateProfileForm props={props}/>
        </Fragment>
    )
};

const CreateProfileForm = ({errors, touched, isSubmitting, setFieldValue, setFieldTouched, values}) => (
    <Form>
        <BootstrapForm.Group className="form-group">
            <Field name="status">{({field}) => (
                <Select
                    {...field}
                    onChange={value => setFieldValue("status", value)}
                    onBlur={() => setFieldTouched("status", true)}
                    value={values.status}
                    placeholder="Job Status *"
                    className="form-control form-control-select"
                >
                    {statusOptions.map((option, i) => (
                        <Option key={i} value={option.value}>{option.label}</Option>
                    ))}
                </Select>
            )}</Field>
            <BootstrapForm.Text className="text-muted">Select ur Job Status *</BootstrapForm.Text>
            {touched.status && errors.status &&
            <BootstrapForm.Text className="text-muted">{errors.status}</BootstrapForm.Text>}
        </BootstrapForm.Group>
        <BootstrapForm.Group className="form-group">
            <Field
                type="text"
                placeholder="Company"
                name="company"
                className="form-control"
            />
            {touched.company && errors.company &&
            <BootstrapForm.Text className="text-muted">{errors.company}</BootstrapForm.Text>}
        </BootstrapForm.Group>
        <BootstrapForm.Group className="form-group">
            <Field
                type="text"
                placeholder="Website"
                name="website"
                className="form-control"
            />
            {touched.website && errors.website &&
            <BootstrapForm.Text className="text-muted">{errors.website}</BootstrapForm.Text>}
        </BootstrapForm.Group>
        <BootstrapForm.Group className="form-group">
            <Field
                type="text"
                placeholder="Location"
                name="location"
                className="form-control"
            />
            {touched.location && errors.location &&
            <BootstrapForm.Text className="text-muted">{errors.location}</BootstrapForm.Text>}
        </BootstrapForm.Group>
        <BootstrapForm.Group className="form-group">
            <Field
                type="text"
                placeholder="Skills *"
                name="skills"
                className="form-control"
            />
            <BootstrapForm.Text className="text-muted">Please use comma separated values (eg. HTML,CSS,JavaScript,PHP)</BootstrapForm.Text>
            {touched.skills && errors.skills &&
            <BootstrapForm.Text className="text-muted">{errors.skills}</BootstrapForm.Text>}
        </BootstrapForm.Group>
        <BootstrapForm.Group className="form-group">
            <Field
                type="text"
                as="textarea"
                placeholder="A short bio of yourself"
                name="bio"
                className="form-control"
            />
            {touched.bio && errors.bio &&
            <BootstrapForm.Text className="text-muted">{errors.bio}</BootstrapForm.Text>}
        </BootstrapForm.Group>
        <InputGroup className="form-group">
            <InputGroup.Prepend>
                <InputGroup.Text><FontAwesomeIcon icon={faGithub}/></InputGroup.Text>
            </InputGroup.Prepend>
            <Field
                type="text"
                placeholder="Github Username"
                name="githubusername"
                className="form-control"
            />
            {touched.githubusername && errors.githubusername &&
            <BootstrapForm.Text className="text-muted">{errors.githubusername}</BootstrapForm.Text>}
        </InputGroup>
        <InputGroup className="form-group">
            <InputGroup.Prepend>
                <InputGroup.Text><FontAwesomeIcon icon={faTwitter}/></InputGroup.Text>
            </InputGroup.Prepend>
            <Field
                type="text"
                placeholder="Twitter URL"
                name="twitter"
                className="form-control"
            />
            {touched.twitter && errors.twitter &&
            <BootstrapForm.Text className="text-muted">{errors.twitter}</BootstrapForm.Text>}
        </InputGroup>
        <InputGroup className="form-group">
            <InputGroup.Prepend>
                <InputGroup.Text><FontAwesomeIcon icon={faFacebook}/></InputGroup.Text>
            </InputGroup.Prepend>
            <Field
                type="text"
                placeholder="Facebook URL"
                name="facebook"
                className="form-control"
            />
            {touched.facebook && errors.facebook &&
            <BootstrapForm.Text className="text-muted">{errors.facebook}</BootstrapForm.Text>}
        </InputGroup>
        <InputGroup className="form-group">
            <InputGroup.Prepend>
                <InputGroup.Text><FontAwesomeIcon icon={faInstagram}/></InputGroup.Text>
            </InputGroup.Prepend>
            <Field
                type="text"
                placeholder="Instagram URL"
                name="instagram"
                className="form-control"
            />
            {touched.instagram && errors.instagram &&
            <BootstrapForm.Text className="text-muted">{errors.instagram}</BootstrapForm.Text>}
        </InputGroup>
        <InputGroup className="form-group">
            <InputGroup.Prepend>
                <InputGroup.Text><FontAwesomeIcon icon={faLinkedin}/></InputGroup.Text>
            </InputGroup.Prepend>
            <Field
                type="text"
                placeholder="Linkedin URL"
                name="linkedin"
                className="form-control"
            />
            {touched.linkedin && errors.linkedin &&
            <BootstrapForm.Text className="text-muted">{errors.linkedin}</BootstrapForm.Text>}
        </InputGroup>
        <InputGroup className="form-group">
            <InputGroup.Prepend>
                <InputGroup.Text><FontAwesomeIcon icon={faYoutube}/></InputGroup.Text>
            </InputGroup.Prepend>
            <Field
                type="text"
                placeholder="YouTube URL"
                name="youtube"
                className="form-control"
            />
            {touched.youtube && errors.youtube &&
            <BootstrapForm.Text className="text-muted">{errors.youtube}</BootstrapForm.Text>}
        </InputGroup>
        <Button type="submit" disabled={isSubmitting}>Submit</Button>
    </Form>
);

const FormikCreateProfileForm = withFormik({
    mapPropsToValues() {
        return {
            company: '',
            website: '',
            location: '',
            status: '',
            skills: '',
            githubusername: '',
            bio: '',
            twitter: '',
            facebook: '',
            linkedin: '',
            youtube: '',
            instagram: ''
        }
    },
    validationSchema: Yup.object().shape({
        status: Yup.string().required('Give us an idea of where you are at in your career'),
        skills: Yup.string().required('Please enter ur skills')
    }),
    handleSubmit(values, {setSubmitting, resetForm, props}) {
        props.props.createProfile(values, props.props.history);
        if (props.props.isAuthenticated === false) {
            setSubmitting(false);
        }
    }
})(CreateProfileForm);

CreateProfile.propTypes = {
    createProfile: PropTypes.func.isRequired,
    isAuthenticated: PropTypes.bool
};

const mapStateToProps = state => ({
    isAuthenticated: state.auth.isAuthenticated
});

export default connect(mapStateToProps, {createProfile})(withRouter(CreateProfile));