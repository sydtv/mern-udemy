import React, {Fragment} from 'react';
import {Link, Redirect} from "react-router-dom";
import {connect} from 'react-redux';
import {Form as BootstrapForm, Button} from 'react-bootstrap';
import {withFormik, Form, Field} from 'formik';
import {setAlert} from '../../actions/alert';
import {register} from '../../actions/auth';
import PropTypes from 'prop-types';
import * as Yup from 'yup';

const Register = (props) => {

    if(props.isAuthenticated) {
        return <Redirect to='/dashboard'/>
    }

    return (
        <Fragment>
            <h1 className='mb-4'>Create Your Account</h1>
            <FormikRegisterForm props={props} />
            <p className="my-1">
                <Link to='/login'>Already have an account?</Link>
            </p>
        </Fragment>
    )
};

const RegisterForm = ({errors, touched, isSubmitting}) => (
    <Form>
        <BootstrapForm.Group>
            <Field
                type="text"
                placeholder="Name"
                name="name"
                className="form-control"
            />
            {touched.name && errors.name &&
            <BootstrapForm.Text className="text-muted">{errors.name}</BootstrapForm.Text>}
        </BootstrapForm.Group>
        <BootstrapForm.Group className="form-group">
            <Field
                type="email"
                placeholder="Email Address"
                name="email"
                className="form-control"
            />
            {touched.email && errors.email &&
            <BootstrapForm.Text className="text-muted">{errors.email}</BootstrapForm.Text>}
            <small className="form-text">
                This site uses Gravatar so if you want a profile image, use a
                Gravatar email
            </small>
        </BootstrapForm.Group>
        <BootstrapForm.Group className="form-group">
            <Field
                type="password"
                placeholder="Password"
                name="password"
                minLength="6"
                className="form-control"
            />
            {touched.password && errors.password &&
            <BootstrapForm.Text className="text-muted">{errors.password}</BootstrapForm.Text>}
        </BootstrapForm.Group>
        <BootstrapForm.Group className="form-group">
            <Field
                type="password"
                placeholder="Confirm Password"
                name="passwordConfirmation"
                minLength="6"
                className="form-control"
            />
            {touched.passwordConfirmation && errors.passwordConfirmation &&
            <BootstrapForm.Text className="text-muted">{errors.passwordConfirmation}</BootstrapForm.Text>}
        </BootstrapForm.Group>
        <Button type="submit" disabled={isSubmitting}>Submit</Button>
    </Form>
);

const FormikRegisterForm = withFormik({
    mapPropsToValues() {
        return {
            name: '',
            email: '',
            password: '',
            passwordConfirmation: ''
        }
    },
    validationSchema: Yup.object().shape({
        name: Yup.string().required('Name is a required field.'),
        email: Yup.string().email('Email needs to be a valid email.').required('Email is a required field.'),
        // eslint-disable-next-line
        password: Yup.string().min(8, 'The password must be 8 signs long.').required('Password is a required field.').matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/, 'Password needs to contain at least one capital letter, one small letter, a number and a special sign.'),
        passwordConfirmation: Yup.string().min(8, 'The password must be 8 signs long.').required('Password Confirmation is a required field.').oneOf([Yup.ref('password'), null], 'The passwords must match')
    }),
    handleSubmit(values, {setSubmitting, resetForm, props}) {
        const {name, email, password} = values;
        props.props.register({name, email, password});
        if(props.props.isAuthenticated) {
            resetForm();
            return <Redirect to='/dashboard'/>
        }
        setSubmitting(false);
    }
})(RegisterForm);

Register.propTypes = {
    setAlert: PropTypes.func.isRequired,
    register: PropTypes.func.isRequired,
    isAuthenticated: PropTypes.bool
};

const mapStateToProps = state => ({
    isAuthenticated: state.auth.isAuthenticated
});

export default connect(mapStateToProps, {setAlert, register})(Register);