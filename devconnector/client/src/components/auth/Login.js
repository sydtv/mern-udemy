import React, {Fragment} from 'react';
import {Link, Redirect} from "react-router-dom";
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {login} from "../../actions/auth";
import {Form as BootstrapForm, Button} from 'react-bootstrap';
import {withFormik, Form, Field} from 'formik';
import * as Yup from 'yup';

const Login = (props) => {

    if (props.isAuthenticated) {
        return <Redirect to='/dashboard'/>
    }

    return (
        <Fragment>
            <h1>Sign in</h1>
            <FormikLoginForm props={props}/>
            <p className="my-1">
                <Link to='/register'>Don't have an account?</Link>
            </p>
        </Fragment>
    )
};

const LoginForm = ({errors, touched, isSubmitting}) => (
    <Form>
        <BootstrapForm.Group className="form-group">
            <Field
                type="email"
                placeholder="Email Address"
                name="email"
                className="form-control"
            />
            {touched.email && errors.email &&
            <BootstrapForm.Text className="text-muted">{errors.email}</BootstrapForm.Text>}
        </BootstrapForm.Group>
        <BootstrapForm.Group className="form-group">
            <Field
                type="password"
                placeholder="Password"
                name="password"
                minLength="6"
                className="form-control"
            />
            {touched.password && errors.password &&
            <BootstrapForm.Text className="text-muted">{errors.password}</BootstrapForm.Text>}
        </BootstrapForm.Group>
        <Button type="submit" disabled={isSubmitting}>Submit</Button>
    </Form>
);

const FormikLoginForm = withFormik({
    mapPropsToValues() {
        return {
            email: '',
            password: ''
        }
    },
    validationSchema: Yup.object().shape({
        email: Yup.string().email('Email needs to be a valid email.').required('Email is a required field.'),
        // eslint-disable-next-line
        password: Yup.string().min(8, 'The password must be 8 signs long.').required('Password is a required field.').matches(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/, 'Password needs to contain at least one capital letter, one small letter, a number and a special sign.')
    }),
    handleSubmit(values, {setSubmitting, resetForm, props}) {
        const {email, password} = values;
        props.props.login(email, password);
        if (props.props.isAuthenticated === false) {
            setSubmitting(false);
        }
    }
})(LoginForm);

Login.propTypes = {
    login: PropTypes.func.isRequired,
    isAuthenticated: PropTypes.bool
};

const mapStateToProps = state => ({
    isAuthenticated: state.auth.isAuthenticated
});

export default connect(mapStateToProps, {login})(Login);