import React, {useEffect, Fragment} from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom'
import {connect} from 'react-redux';
import Spinner from './../layout/Spinner';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faUser} from '@fortawesome/free-solid-svg-icons';
import {getCurrentProfile} from "../../actions/profile";

const Dashboard = ({getCurrentProfile, auth: {isAuthenticated, loading, user}, profile}) => {
    useEffect(() => {
        getCurrentProfile();
    }, [getCurrentProfile]);

    return loading && profile === null ? <Spinner/> : <Fragment>
        <h1>Dashboard</h1>
        {user && user.name ? <p className="lead"><FontAwesomeIcon icon={faUser}/> {user.name}</p> : null}
        {profile.profile !== null ?
            <Fragment>has</Fragment> :
            <Fragment>
                <p>You have not setup a profile, please create one.</p>
                <Link to='/create-profile' className="btn btn-primary">Create Profile</Link>
            </Fragment>}
    </Fragment>
};

Dashboard.propTypes = {
    getCurrentProfile: PropTypes.func.isRequired,
    auth: PropTypes.object.isRequired,
    profile: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
    auth: state.auth,
    profile: state.profile
});

export default connect(mapStateToProps, {getCurrentProfile})(Dashboard);